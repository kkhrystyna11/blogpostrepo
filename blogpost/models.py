from django.db import models
from django.contrib.auth.models import User
#from PIL import Image
#from django.template.defaultfilters import slugify


# Create your models here.
class Posts(models.Model):
    user = models.ForeignKey(User, default=None, on_delete=models.PROTECT)
    title = models.CharField(max_length=100)
    post = models.TextField()
    date = models.DateTimeField(auto_now_add=True)
    image = models.ImageField(default='default.png', blank=True)


def __str__(self):
    return self.title

