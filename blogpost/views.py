from django.shortcuts import render, redirect, render_to_response, get_object_or_404
from django.contrib import auth, messages
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from .models import Posts
from . import forms
import json
from django.http import JsonResponse
from django.template import loader


# Create your views here.
def home(request):
    """
    Checks if request comes from authenticated user or not,
    returns user to either personal page or standard home page
    """
    if request.user.is_authenticated:
        return HttpResponseRedirect('/blogpost/loggedin/')
    else:
        return render(request, 'blogpost/home.html')


def login(request):
    """
    Checks if request comes from authenticated user or not,
    returns either personal page or standard login page
    """
    if request.user.is_authenticated:
        return HttpResponseRedirect('/blogpost/loggedin/')
    else:
        return render(request, 'blogpost/login.html')


def register(request):
    """
    Checks if request comes from authenticated user or not,
    GET: returns either personal page or registration page with registration form
    POST: checks if form is valid, sends data to server and redirects to register_success
    """
    if request.user.is_authenticated:
        return HttpResponseRedirect('/blogpost/loggedin/')
    else:
        if request.method == 'POST':
            form = UserCreationForm(request.POST)
            if form.is_valid():
                form.save()
                return HttpResponseRedirect("/blogpost/register_success/")
            else:
                form = UserCreationForm()
    args = {}
    args['form'] = UserCreationForm()
    return render_to_response('blogpost/register.html', args)


def register_success(request):
    """
    Renders register_success page
    """
    return render(request, 'blogpost/register_success.html', {})


def auth_view(request):
    """
    Receives POST request with login details
    Redirects to personal page or invalid_login
    """
    username = request.POST.get('username', '')
    password = request.POST.get('password', '')

    user = auth.authenticate(username=username, password=password)

    if user is not None:
        auth.login(request, user)
        return HttpResponseRedirect('/blogpost/loggedin/')
    else:
        return HttpResponseRedirect('/blogpost/invalid/')


@login_required
def loggedin(request):
    """
    Checks user's details
    Returns data related to active user
    """
    posts = request.user.posts_set.all()
    username = request.user.username
    return render(request, 'blogpost/loggedin.html', {'username': username, 'posts': posts})


def invalid_login(request):
    """
    Renders invalid_login page
    """
    return render(request, 'blogpost/invalid_login.html', {})


def logout(request):
    """
    Handles logout
    """
    auth.logout(request)
    return render(request, 'blogpost/logout.html', {})


@login_required
def create_post(request):
    """
    Checks if request is ajax
    POST: checks if form is valid, saves it, creates JSON with filled in data, returns it as a response
    """
    if request.is_ajax():
        message = 'Ajax Magic'
        if request.method == 'POST':
            form = forms.PostForm(request.POST, request.FILES)
            if form.is_valid:
                instance = form.save(commit=False)
                instance.user = request.user
                instance.save()
                files = request.FILES
                if files:
                    image_file = json.dumps(str(files['image'])).strip('\"')
                else:
                    image_file = 'default.png'
                post_title = request.POST['title']
                post_post = request.POST['post']
                payload = {'json_title': post_title, 'json_post': post_post, 'json_image': image_file}
                print(payload)
                return JsonResponse({'json_title': post_title, 'json_post': post_post, 'json_image': image_file,
                                     'message': message + ' Uploaded Post Successfully!'})
            else:
                return JsonResponse({'error': True, 'errors': form.errors})
        else:
            form = forms.PostForm()
            return render(request, "/blogpost/loggedin.html", {'form': form})
    else:
        message = 'Sorry, Ajax does not listen to you'
        return JsonResponse({'message': message})


@login_required
def delete_posts(request, pk):
    """
    Deletes specific post
    """
    title = get_object_or_404(Posts, id=pk)
    title.delete()
    return HttpResponseRedirect('/blogpost/loggedin/')


@login_required
def posts(request):
    """
    Loads all posts on Home Page
    """
    posts = Posts.objects.all()
    return render(request, 'blogpost/posts.html', {'posts': posts})


@login_required
def post_load(request):
    """
    Loads posts on personal page
    """
    posts = Posts.objects.all()
    posts_html = loader.render_to_string('blogpost/post_load.html', {'posts': posts})
    return JsonResponse(posts_html, safe=False)
