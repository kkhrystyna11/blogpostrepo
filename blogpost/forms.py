from django import forms
#from .models import Posts
from . import models


class PostForm(forms.ModelForm):
   class Meta:
       model = models.Posts
       fields = ['title', 'post', 'image']

# def myview(self):
#     if request.method == 'POST':
#         form = PostForm(request.POST, request.FILES)
#         if form.is_valid():
#             form.save()
#     else:
#         form = PostForm()
#     return render(request, '/blogpost/loggedin.html', {'form': form})
