// Javascript - JQuery additional functionality //
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
            // Only send the token to relative URLs i.e. locally.
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }
});

$(document).ready(function() {
    $("#clear").on('click', function() {
        if (confirm("Are you sure you want to clear this post?")) {
            $('#title').val('');
            $('#post').val('');
            $('#image').val('');
        } else {
            return false;
        }
    });

    $('#post_form').on('submit', function(event) {
        event.preventDefault();
        $form = $(this);
        var formData = new FormData(this);

        $.ajax({
            url: $('#post_form').attr('action'),
            type: 'POST',
            data: formData,
            cache: false,
            processData: false,
            contentType: false,
            success: function(response) {
                $('.error').remove();
                console.log(response);
                if (response.error) {
                    $.each(response.errors, function(name, error) {
                        error = '<small class="text-muted error">' + error + '</small>'
                        $form.find('[name=' + name + ']').after(error);
                    })
                } else {
                    alert(response.message)
                    $("#post_form")[0].reset();
                    //  $(".container-fluid").empty();
                    console.log(response.json_title);
                    var title = response.json_title;
                    var post = response.json_post;
                    var image = response.json_image;
                    var postvar = `<div class="container"> \
                                <div class="post-load" id="post-load"> \
                                  <div class="row"> \
                                  <div class="col-lg-3 col-md-3 form-group"> \
      			                    <img class="img-fluid float-left" src="/media/` +
                        image +
                        `" alt="Image not found"/> \
      			                  </div> \
				                  <div class="col-lg-7 col-md-7 form-group" style="text-align:left"> \
				                    <h4>` +
                        title +
                        `</h4> \
			                        <p>` +
                        post +
                        `</p> \
			                      </div> \
				                  <div class="col-lg-2 col-md-2 form-group" style="text-align:right"> \
				                    <a href = "{% url 'delete_posts' pk=post.pk %}">
			                          <button type="button" class="btn btn-secondary">Delete</button>
				                    </a>
				                  </div>
     			                </div> \
			                  </div>`;
                    console.log(postvar);
                    $("#posts_list").append(postvar);
                }
            }
        });
    });

});