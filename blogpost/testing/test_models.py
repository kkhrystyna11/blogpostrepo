from mixer.backend.django import mixer
from django.contrib.auth.models import User
from blogpost.models import Posts
import datetime
import pytest


@pytest.mark.django_db
class TestModels:

    def setup(self):
        self.test_user = User.objects.create_user(username="test", password="123test")
        self.test_post = Posts.objects.create(
            user=self.test_user,
            title="Test Post",
            post="This is simply testing post",
            date=datetime.datetime.now(),
            image="default.png")

    def test_post(self):
        assert isinstance(self.test_post, Posts)
