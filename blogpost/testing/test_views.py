from django.test import RequestFactory
from django.contrib.auth.models import AnonymousUser, User
from django.urls import reverse
from blogpost.views import home, posts, loggedin, create_post
import pytest


@pytest.mark.django_db
class TestAuthView:

    def setup(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='test', password='123test')

    def test_posts(self):
        path = reverse('posts')
        request = self.factory.get(path)
        request.user = self.user
        response = posts(request)
        assert response.status_code == 200

    def test_loggedin(self):
        path = reverse('loggedin')
        request = self.factory.get(path)
        request.user = self.user
        response = loggedin(request)
        assert response.status_code == 200


@pytest.mark.django_db
class TestNonAuthUser:

    def setup(self):
        self.factory = RequestFactory()

    def test_login_required(self):
        path = reverse('posts')
        request = self.factory.get(path)
        request.user = AnonymousUser
        response = posts(request)
        assert response.status_code == 200


@pytest.mark.django_db
class TestPost:

    def setup(self):
        self.factory = RequestFactory()
        self.user = User.objects.create_user(username='test', password='123test')

    def test_new_post(self):
        path = reverse('create_post')
        form_data = {
            'user': 'self.test_user',
            'title': 'Test Post',
            'post': 'This is simply testing post',
            'date': 'datetime.datetime.now()',
            'image': 'default.png'
        }
        request = self.factory.post(path, data=form_data)
        request.user = self.user
        response = create_post(request)
        assert response.status_code == 200
